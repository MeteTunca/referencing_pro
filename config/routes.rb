Rails.application.routes.draw do
 
  resources :sourcetypes
  resources :referenceauthors
  resources :authors
  resources :references
   
  get 'static_pages/show'
  root 'static_pages#show'
  get ':page', to: 'static_pages#show', as: 'pages'





  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

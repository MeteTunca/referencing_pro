class CreateReferenceauthors < ActiveRecord::Migration[5.0]
  def change
    create_table :referenceauthors do |t|
      t.integer :author_id
      t.integer :reference_id

      t.timestamps
    end
  end
end

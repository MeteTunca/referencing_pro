class CreateReferences < ActiveRecord::Migration[5.0]
  def change
    create_table :references do |t|
      t.integer :sourcetype_id
      t.datetime :publicationYear
      t.string :title
      t.string :series
      t.string :edition
      t.string :publicationplace
      t.string :publisher
      t.string :webadress
      t.datetime :accessdate
      t.string :titleofconference

      t.timestamps
    end
  end
end

require 'test_helper'

class ReferenceauthorsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @referenceauthor = referenceauthors(:one)
  end

  test "should get index" do
    get referenceauthors_url
    assert_response :success
  end

  test "should get new" do
    get new_referenceauthor_url
    assert_response :success
  end

  test "should create referenceauthor" do
    assert_difference('Referenceauthor.count') do
      post referenceauthors_url, params: { referenceauthor: { author_id: @referenceauthor.author_id, reference_id: @referenceauthor.reference_id } }
    end

    assert_redirected_to referenceauthor_url(Referenceauthor.last)
  end

  test "should show referenceauthor" do
    get referenceauthor_url(@referenceauthor)
    assert_response :success
  end

  test "should get edit" do
    get edit_referenceauthor_url(@referenceauthor)
    assert_response :success
  end

  test "should update referenceauthor" do
    patch referenceauthor_url(@referenceauthor), params: { referenceauthor: { author_id: @referenceauthor.author_id, reference_id: @referenceauthor.reference_id } }
    assert_redirected_to referenceauthor_url(@referenceauthor)
  end

  test "should destroy referenceauthor" do
    assert_difference('Referenceauthor.count', -1) do
      delete referenceauthor_url(@referenceauthor)
    end

    assert_redirected_to referenceauthors_url
  end
end

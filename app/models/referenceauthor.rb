class Referenceauthor < ApplicationRecord
   belongs_to :reference
   belongs_to :author
  
   def self.get_first_letter(str)
         str[0,1]
   end
   
   #Create Harvard Style Bibliography based on source tyoe
   def self.get_harvard_referencing(sourcetype,alastname,aname,publicationyear,title,series,edition,publicationplace,publisher,webaddress,accessdate,titleofconference)
   
      #Edit Series String
      unless series.to_s.strip.empty?
      series=series+" Series."
      end
      
      #Edit Edition String
      unless edition.to_s.strip.empty?
      edition=edition+" ed."
      end
      
      #Create Bibliography in Harvard Style for Books
      if(sourcetype=="Book")
         result=(alastname+", "+aname[0,1]+". ("+publicationyear.year.to_s+") <strong>"+title+"</strong> "+" "+series+" "+edition+" "+publicationplace+": "+publisher+".").html_safe
      end
      if(sourcetype=="Website")
         result=(aname+" ("+publicationyear.year.to_s+") "+title+" [Online] Available at: <"+ webaddress +"> [Accessed "+accessdate.day.to_s+" "+accessdate.month.to_s+" "+accessdate.year.to_s+"].")
      end
      if(sourcetype=="Conference Paper")
         result=(alastname+", "+aname[0,1]+". ("+publicationyear.year.to_s+") "+title+" ln: "+"<strong>"+titleofconference+"</strong> "+publicationplace+": "+publisher+".").html_safe
      end
      result
   end
 
end

class Author < ApplicationRecord
   has_many :referenceauthors, dependent: :destroy
   has_many :references, through: :referenceauthors
end

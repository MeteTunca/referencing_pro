class Reference < ApplicationRecord
   has_many :referenceauthors, dependent: :destroy
   has_many :authors, through: :referenceauthors
   belongs_to :sourcetype
end

class ReferenceauthorsController < ApplicationController
  before_action :set_referenceauthor, only: [:show, :edit, :update, :destroy]

  # GET /referenceauthors
  # GET /referenceauthors.json
  def index
    @referenceauthors = Referenceauthor.all
  end

  # GET /referenceauthors/1
  # GET /referenceauthors/1.json
  def show
  end

  # GET /referenceauthors/new
  def new
    @referenceauthor = Referenceauthor.new
  end

  # GET /referenceauthors/1/edit
  def edit
  end

  # POST /referenceauthors
  # POST /referenceauthors.json
  def create
    @referenceauthor = Referenceauthor.new(referenceauthor_params)

    respond_to do |format|
      if @referenceauthor.save
        format.html { redirect_to @referenceauthor, notice: 'Referenceauthor was successfully created.' }
        format.json { render :show, status: :created, location: @referenceauthor }
      else
        format.html { render :new }
        format.json { render json: @referenceauthor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /referenceauthors/1
  # PATCH/PUT /referenceauthors/1.json
  def update
    respond_to do |format|
      if @referenceauthor.update(referenceauthor_params)
        format.html { redirect_to @referenceauthor, notice: 'Referenceauthor was successfully updated.' }
        format.json { render :show, status: :ok, location: @referenceauthor }
      else
        format.html { render :edit }
        format.json { render json: @referenceauthor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /referenceauthors/1
  # DELETE /referenceauthors/1.json
  def destroy
    @referenceauthor.destroy
    respond_to do |format|
      format.html { redirect_to referenceauthors_url, notice: 'Referenceauthor was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_referenceauthor
      @referenceauthor = Referenceauthor.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def referenceauthor_params
      params.require(:referenceauthor).permit(:author_id, :reference_id)
    end
end

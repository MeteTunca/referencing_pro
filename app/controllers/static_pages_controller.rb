class StaticPagesController < ApplicationController
 before_action :page
  
  def show
    render page
  end
  
  private
  
  def page
    params.has_key?(:page) ? 
    page = 'static_pages/' + params[:page] : 
    page = 'static_pages/show'
  end   

end

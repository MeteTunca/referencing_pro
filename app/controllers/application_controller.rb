class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  
   before_action :set_locale

  rescue_from ActiveRecord::RecordNotFound, with: :render_not_found_response
  
  def render_not_found_response
    redirect_to root_path, notice: 'Record not found!'
  end
  
  private
  
  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end
  
  def default_url_options
    { locale: I18n.locale }
  end
  
end

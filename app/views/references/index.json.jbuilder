json.array!(@references) do |reference|
  json.extract! reference, :id, :sourcetype_id, :publicationYear, :title, :series, :edition, :publicationplace, :publisher, :webadress, :accessdate, :titleofconference
  json.url reference_url(reference, format: :json)
end

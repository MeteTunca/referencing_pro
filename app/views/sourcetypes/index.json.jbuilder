json.array!(@sourcetypes) do |sourcetype|
  json.extract! sourcetype, :id, :name
  json.url sourcetype_url(sourcetype, format: :json)
end

json.array!(@referenceauthors) do |referenceauthor|
  json.extract! referenceauthor, :id, :author_id, :reference_id
  json.url referenceauthor_url(referenceauthor, format: :json)
end

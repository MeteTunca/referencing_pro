json.array!(@authors) do |author|
  json.extract! author, :id, :name, :lastname
  json.url author_url(author, format: :json)
end

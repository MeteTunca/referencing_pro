# README
- This is the main project which provide Bibliography service in two different languages (English and Turkish)
- Project has 4 models. Models have one-to-many and many-to-many relation.
- Steps have been executed shown below step by step.
- Create new project called "referencing_pro"
rails _5.0.1_ new referencing_pro
- Install necessary bundles
bundle install --without production
bundle update
- Initialize Git, add first commit and push to Bitbucket
git init
git add -A
git commit -m "Initialize repository"
git remote add origin git@bitbucket.org:MeteTunca/referencing_pro.git
git push -u origin --all
- Create Static pages and push git
rails generate controller StaticPages show aboutus
git add -A
git commit -m "Add a Static Pages controller"
git push -u origin static-pages
- Create Author entity and migrate
rails generate scaffold Author name:string lastname:string
rails db:migrate
- Create Sourcetype entity and migrate
rails generate scaffold Sourcetype name
rails db:migrate
- Create Reference entity with one-to-many relation with Sourcetype entity and migrate
rails generate scaffold Reference sourcetype_id:integer publicationYear:datetime title:string series:string edition:string publicationplace:string publisher:string webadress:string accessdate:datetime titleofconference:string 
rails db:migrate
- Create ReferenceAuthor entity with many-to-many relation with Author and Reference entities and migrate
rails generate scaffold Referenceauthor author_id:integer reference_id:integer
rails db:migrate

edit routing file
edit views
build business methods in models

- Create branch for layout work
git checkout -b filling-in-layout
- Install Bootstrap
gem 'bootstrap-sass', '3.3.6'
- Crete custom.scss in assests/stylesheets folder and partials such as header,footer in layouts folder
- css codes applied
-Internationalisation Work
Add gem 'rails-i18n' into Gemfile
bundle install --without production
- Create def set_locale method into ApplicationController
- Create en.yml and tr.yml in locales folder
- Add static and activerecord translations to tr.yml 
- Add Translation method to menu items in Layouts/Application.html.erb and header, footer partials
- Create Turkish static pages such as aboutus.tr, help.tr, show.tr
- Add Translation method to Activerecord items such as show,edit,destroy,new
- 








    
   
   